<?php

/* @var $this yii\web\View */

$this->title = 'Inicio - Flores';
?>

<div class="site-index">
    <div class="body-content">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, laudantium, officiis, dignissimos, tempore asperiores numquam minus culpa nisi aliquam cum libero ducimus assumenda quae maxime perspiciatis vel illo. Veritatis, architecto!
        
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, aliquam, dolores tempora nulla culpa et consequatur quidem quo quia alias ipsa quae sequi dolorum consequuntur accusantium laudantium eos cumque mollitia.
        
        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, illo eveniet architecto facilis obcaecati vel! Commodi, facilis, quis, dolores, nam eos nesciunt illum tenetur alias quae placeat nostrum adipisci maxime.</div>
        <div>Sed, reiciendis, debitis, dolorum, quis fugit ab molestias distinctio dolore architecto recusandae ipsa dignissimos rerum! Rem, numquam quaerat eaque suscipit voluptate ipsa minus earum amet temporibus illo perspiciatis perferendis asperiores.</div>
        <div>Exercitationem, maiores, amet. Porro, dolorum, voluptate, quo omnis asperiores placeat ea architecto non repudiandae nihil earum et laboriosam eveniet maiores possimus eum dolor fugiat consequatur error cum perferendis voluptates unde.</div>
        <div>Unde, nulla, ratione aut culpa ipsam nisi architecto eum repudiandae vero ea omnis totam possimus veritatis animi repellendus numquam distinctio quasi cum velit harum maiores dolorum nobis minima maxime reiciendis.</div>
        <div>Iure, voluptatibus, dolor, expedita, adipisci distinctio maxime quisquam perferendis veniam atque earum temporibus sunt sed nam voluptate quia blanditiis praesentium nostrum tenetur ipsum pariatur. Quidem, quam quaerat modi esse accusamus.</div>
    </div>
</div>
